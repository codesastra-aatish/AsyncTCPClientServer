/**
 * Asyncronous Client/Server TCP Application
 * Server side program
 *
 * @author Aatish Sai
 * @verison 1.0
 * @since 2016-5-5
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Server{

	public static void main(String args[]){

		ServerSocket sSocket;
		Socket cSocket;

		DataInputStream in;
		DataOutputStream out;

		Scanner scan = new Scanner(System.in);
		try{
			sSocket = new ServerSocket(6789);
			cSocket = sSocket.accept();

			out = new DataOutputStream(cSocket.getOutputStream());
			in = new DataInputStream(cSocket.getInputStream());

			//Thread to send the message
			Thread send = new Thread(new Runnable() {
				String msg;
				@Override
				public void run() {
					try{
						while(true){
							msg = scan.nextLine();
							out.writeUTF(msg);
						}
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			});
			send.start();

			//Thread to receive message
			Thread receive = new Thread(new Runnable() {
				String msg;
				@Override
				public void run() {
					try{
						msg = in.readUTF();
						while(msg != null){
							System.out.println("Client : "+msg);
							msg = in.readUTF();
						}
						out.close();
						cSocket.close();
						sSocket.close();
					}catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			receive.start();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
