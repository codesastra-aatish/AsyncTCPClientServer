/**
 * Asyncronous Client/Server TCP Application
 * Client side program
 *
 * @author Aatish Sai
 * @version 1.0
 * @since 2016-5-5
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Client{

	public static void main(String args[]){
		Socket cSocket;
		DataInputStream in;
		DataOutputStream out;

		Scanner scan = new Scanner(System.in);

		//supply host address as our first argument
		String host = args[0];
		System.out.println(host);

		try{
			cSocket = new Socket(host,6789);

			out = new DataOutputStream(cSocket.getOutputStream());
			in = new DataInputStream(cSocket.getInputStream());

			//Thread to send the message
			Thread send = new Thread(new Runnable() {
				String msg;
				@Override
				public void run() {
					try{
						while(true){
							//scan the text from the console until the user press enter
							msg = scan.nextLine();
							//send the scanned string using output stream
							out.writeUTF(msg);
						}
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			});
			send.start();

			//Thread to receive message
			Thread receive = new Thread(new Runnable() {
				String msg;
				@Override
				public void run() {
					try {
						//read message from the input stream
						msg = in.readUTF();
						while(msg != null){
							System.out.println("Server :"+msg);
							msg = in.readUTF();
						}
						out.close();
						cSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			receive.start();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
